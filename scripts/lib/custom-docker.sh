PATH_TOOLS=(DEBIAN RUBY GOLANG NODE POSTGRESQL)
TAG_TOOLS=(BUNDLER GIT LFS CHROME YARN GRAPHICSMAGICK PGBOUNCER BAZELISK DOCKER BUILDX GCLOUD KUBECTL)

function get_image_path() {
    local path
    path=""
    for tool in "${PATH_TOOLS[@]}"; do
        if [[ -n "${!tool}" ]]; then
            path="${path}-${tool,,}-${!tool}"
        fi
    done

    if [[ -n "$path" ]]; then
        echo "$CI_REGISTRY_IMAGE/${path:1}"
    else
        echo "$CI_REGISTRY_IMAGE"
    fi
}

function get_image_tag() {
    local tag
    tag=""
    for tool in "${TAG_TOOLS[@]}"; do
        if [[ -n "${!tool}" ]]; then
            tag="${tag}-${tool,,}-${!tool}"
        fi
    done
    
    if [[ -n "$tag" ]]; then
        echo "${tag:1}"
    else
        echo "latest"
    fi
}

#!/bin/bash
set -e
IFS=$'\n\t'

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source "$SCRIPT_DIR/custom-docker.sh"

function print_golang_args() {
    declare -A GOLANG_DOWNLOAD_SHA256

    case "$1" in
        1.16)
            INSTALL_GOLANG_VERSION=1.16.12
            GOLANG_DOWNLOAD_SHA256[amd64]=7d657e86493ac1d5892f340a7d88b862b12edb5ac6e73c099e8e0668a6c916b7
            GOLANG_DOWNLOAD_SHA256[arm64v8]=7dbf50ab2e665ecd6c86a3f1ce8c04f7167f9895b91921e25cf1bdc1cb9b5fd7
            ;;
        1.17)
            INSTALL_GOLANG_VERSION=1.17.10
            GOLANG_DOWNLOAD_SHA256[amd64]=87fc728c9c731e2f74e4a999ef53cf07302d7ed3504b0839027bd9c10edaa3fd
            GOLANG_DOWNLOAD_SHA256[arm64v8]=649141201efa7195403eb1301b95dc79c5b3e65968986a391da1370521701b0c
            ;;
        1.18)
            INSTALL_GOLANG_VERSION=1.18.2
            GOLANG_DOWNLOAD_SHA256[amd64]=e54bec97a1a5d230fc2f9ad0880fcbabb5888f30ed9666eca4a91c5a32e86cbc
            GOLANG_DOWNLOAD_SHA256[arm64v8]=fc4ad28d0501eaa9c9d6190de3888c9d44d8b5fb02183ce4ae93713f67b8a35b
            ;;
        *) echo "Unknown golang version $1"; exit 1;
    esac

    printf -- "--build-arg INSTALL_GOLANG_VERSION=%s " "$INSTALL_GOLANG_VERSION"
    printf -- "--build-arg GOLANG_DOWNLOAD_SHA256=%s " "${GOLANG_DOWNLOAD_SHA256[$CUSTOM_DOCKER_ARCH]}"
}

# If you add a new minor version here, be sure to check that the
# Chrome versions can be found at https://www.ubuntuupdates.org/pm/google-chrome-stable.
# ChromeDriver supports this: https://sites.google.com/chromium.org/driver/downloads
# You may need to bump the version in scripts/install-chrome.
function print_chrome_args() {
    case "$1" in
        94|94.0)
            CHROME_VERSION=94.0.4606.81-1
            CHROME_DRIVER_VERSION=94.0.4606.113
            ;;
        97|97.0)
            CHROME_VERSION=97.0.4692.99-1
            CHROME_DRIVER_VERSION=97.0.4692.71
            ;;
        99|99.0)
            CHROME_VERSION=99.0.4844.74-1
            CHROME_DRIVER_VERSION=99.0.4844.51
            ;;
        101|101.0)
            CHROME_VERSION=101.0.4951.64-1
            CHROME_DRIVER_VERSION=101.0.4951.41
            ;;
        *) echo "Unknown chrome version $1"; exit 1;
    esac
    printf -- "--build-arg CHROME_VERSION=%s " "$CHROME_VERSION"
    printf -- "--build-arg CHROME_DRIVER_VERSION=%s " "$CHROME_DRIVER_VERSION"
}

# see https://www.kernel.org/pub/software/scm/git
function print_git_args() {
    case "$1" in
        2.33)
            GIT_VERSION=2.33.1
            GIT_DOWNLOAD_SHA256=02047f8dc8934d57ff5e02aadd8a2fe8e0bcf94a7158da375e48086cc46fce1d
            ;;
        2.36)
            GIT_VERSION=2.36.1
            GIT_DOWNLOAD_SHA256=37d936fd17c81aa9ddd3dba4e56e88a45fa534ad0ba946454e8ce818760c6a2c
            ;;
        *) echo "Unknown git version $1"; exit 1;
    esac

    case "$GIT_VERSION" in
        *.rc[0-9])
            GIT_DOWNLOAD_URL=https://www.kernel.org/pub/software/scm/git/testing/git-${GIT_VERSION}.tar.gz;;
        *)
            GIT_DOWNLOAD_URL=https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz;;
    esac

    printf -- "--build-arg GIT_VERSION=%s " "$GIT_VERSION"
    printf -- "--build-arg GIT_DOWNLOAD_SHA256=%s " "$GIT_DOWNLOAD_SHA256"
    printf -- "--build-arg GIT_DOWNLOAD_URL=%s " "$GIT_DOWNLOAD_URL"
}

# see https://github.com/git-lfs/git-lfs/releases
function print_lfs_args() {
    declare -A LFS_DOWNLOAD_SHA256

    case "$1" in
        2.9)
            LFS_VERSION=2.9.1
            LFS_DOWNLOAD_SHA256[amd64]=2a8e60cf51ec45aa0f4332aa0521d60ec75c76e485d13ebaeea915b9d70ea466
            LFS_DOWNLOAD_SHA256[arm64v8]=ff2f8472a5ac0e808108bad0cc6be5ca1849eb970228b1aa3d627bcbc8228ad9
            ;;
        *) echo "Unknown Git LFS version $1"; exit 1;
    esac

    printf -- "--build-arg LFS_VERSION=%s " "$LFS_VERSION"
    printf -- "--build-arg LFS_DOWNLOAD_SHA256=%s " "${LFS_DOWNLOAD_SHA256[$CUSTOM_DOCKER_ARCH]}"
}

function print_node_args() {
    case "$1" in
        14.15) NODE_INSTALL_VERSION=14.15.4 ;;
        14|14.16) NODE_INSTALL_VERSION=14.16.0 ;;
        16|16.14) NODE_INSTALL_VERSION=16.14.2 ;;
        *) echo "Unknown node version $1"; exit 1;
    esac
    printf -- "--build-arg NODE_INSTALL_VERSION=%s " "$NODE_INSTALL_VERSION"
}

function print_yarn_args() {
    case "$1" in
        1.12) YARN_INSTALL_VERSION=1.12.3 ;;
        1.16) YARN_INSTALL_VERSION=1.16.0 ;;
        1.21) YARN_INSTALL_VERSION=1.21.1 ;;
        1.22) YARN_INSTALL_VERSION=1.22.17 ;;
        *) echo "Unknown yarn version $1"; exit 1;
    esac
    printf -- "--build-arg YARN_INSTALL_VERSION=%s " "$YARN_INSTALL_VERSION"
}

function print_postgres_args() {
    printf -- "--build-arg POSTGRES_VERSION=%s " "$1"
}

function print_docker_args() {
    printf -- "--build-arg DOCKER_VERSION=%s " "$1"
}

function print_buildx_args() {
    declare -A BUILDX_DOWNLOAD_SHA256

    case "$1" in
        0.8)
            BUILDX_VERSION=0.8.2
            BUILDX_DOWNLOAD_SHA256[amd64]=c64de4f3c30f7a73ff9db637660c7aa0f00234368105b0a09fa8e24eebe910c3
            BUILDX_DOWNLOAD_SHA256[arm64v8]=304d3d9822c75f98ad9cf57f0c234bcf326bbb96d791d551728cadd72a7a377f
            ;;
        *) echo "Unknown buildx version $1"; exit 1;
    esac

    printf -- "--build-arg BUILDX_VERSION=%s " "$BUILDX_VERSION"
    printf -- "--build-arg BUILDX_DOWNLOAD_SHA256=%s " "${BUILDX_DOWNLOAD_SHA256[$CUSTOM_DOCKER_ARCH]}"
}

function print_graphicsmagick_args() {
    case "$1" in
        1.3.29)
            GRAPHISMAGICK_VERSION=1.3.29
            GRAPHISMAGICK_DOWNLOAD_SHA256=de820cd10597205941a7e9d02c2e679231e92e8e769c204ef09034d2279ad453
            ;;
        1.3.33)
            GRAPHISMAGICK_VERSION=1.3.33
            GRAPHISMAGICK_DOWNLOAD_SHA256=00ea0df7c78c903cce325f402429bcd3924168cf39277f743a0641d47c411ee8
            ;;
        1.3.34)
            GRAPHISMAGICK_VERSION=1.3.34
            GRAPHISMAGICK_DOWNLOAD_SHA256=4717f7a32d964c515d83706fd52d34e089c2ffa35f8fbf43c923ce19343cf2f4
            ;;
        1.3.36)
            GRAPHISMAGICK_VERSION=1.3.36
            GRAPHISMAGICK_DOWNLOAD_SHA256=1e6723c48c4abbb31197fadf8396b2d579d97e197123edc70a4f057f0533d563
            ;;
        *) echo "Unknown graphicsmagick version $1"; exit 1;
    esac

    printf -- "--build-arg GRAPHISMAGICK_VERSION=%s " "$GRAPHISMAGICK_VERSION"
    printf -- "--build-arg GRAPHISMAGICK_DOWNLOAD_SHA256=%s " "$GRAPHISMAGICK_DOWNLOAD_SHA256"
}

function print_pgbouncer_args() {
    case "$1" in
       1.14)
            PGBOUNCER_VERSION=1.14.0
            PGBOUNCER_DOWNLOAD_SHA256=a0c13d10148f557e36ff7ed31793abb7a49e1f8b09aa2d4695d1c28fa101fee7
            ;;
        *) echo "Unknown pgbouncer version $1"; exit 1;
    esac

    printf -- "--build-arg PGBOUNCER_VERSION=%s " "$PGBOUNCER_VERSION"
    printf -- "--build-arg PGBOUNCER_DOWNLOAD_SHA256=%s " "$PGBOUNCER_DOWNLOAD_SHA256"
}

function print_bazelisk_args() {
    case "$1" in
        1.9.0)
            BAZELISK_VERSION=1.9.0
            BAZELISK_DOWNLOAD_SHA256=b8c7f2a1b07ad64a2f27f8f19a202f90d044de7b5b6ccc387a6fe5d4a8ec4937
            ;;
        *) echo "Unknown bazelisk version $1"; exit 1;
    esac

    printf -- "--build-arg BAZELISK_VERSION=%s " "$BAZELISK_VERSION"
    printf -- "--build-arg BAZELISK_DOWNLOAD_SHA256=%s " "$BAZELISK_DOWNLOAD_SHA256"
}

function print_ruby_args() {
    case "$1" in
        2.6|2.6.*)
            RUBY_VERSION="2.6.6"
            RUBY_DOWNLOAD_SHA256="364b143def360bac1b74eb56ed60b1a0dca6439b00157ae11ff77d5cd2e92291"
            ;;

        2.7|2.7.patched)
            RUBY_VERSION="2.7.6"
            RUBY_DOWNLOAD_SHA256="e7203b0cc09442ed2c08936d483f8ac140ec1c72e37bb5c401646b7866cb5d10"
            ;;

        # Please update any clients still asking for these images to request `ruby-2.7` instead,
        # which will provide the latest available patch level (see above.)
        2.7.*)
            RUBY_VERSION="2.7.2"
            RUBY_DOWNLOAD_SHA256="6e5706d0d4ee4e1e2f883db9d768586b4d06567debea353c796ec45e8321c3d4"
            ;;

        3.0|3.0.patched)
            RUBY_VERSION="3.0.4"
            RUBY_DOWNLOAD_SHA256="70b47c207af04bce9acea262308fb42893d3e244f39a4abc586920a1c723722b"
            ;;

        *) echo "Unknown ruby version $1"; exit 1;
    esac

    printf -- "--build-arg RUBY_VERSION=%s " "$RUBY_VERSION"
    printf -- "--build-arg RUBY_DOWNLOAD_SHA256=%s " "$RUBY_DOWNLOAD_SHA256"
}

function print_bundler_args() {
    case "$1" in
        2.1)
            BUNDLER_VERSION=2.1.4
            ;;
        2.3)
            BUNDLER_VERSION=2.3.12
            ;;
        *) echo "Unknown bundler version $1"; exit 1;
    esac

    printf -- "--build-arg BUNDLER_VERSION=%s " "$BUNDLER_VERSION"
}

function print_gcloud_args() {
    case "$1" in
        383)
            GCLOUD_VERSION=383.0.1
            ;;
        *) echo "Unknown gcloud version $1"; exit 1;
    esac

    printf -- "--build-arg GCLOUD_VERSION=%s " "$GCLOUD_VERSION"
}

function print_kubectl_args() {
    declare -A KUBECTL_DOWNLOAD_SHA256

    case "$1" in
        1.23)
            KUBECTL_VERSION=1.23.0
            KUBECTL_DOWNLOAD_SHA256[amd64]=2d0f5ba6faa787878b642c151ccb2c3390ce4c1e6c8e2b59568b3869ba407c4f
            KUBECTL_DOWNLOAD_SHA256[arm64]=1d77d6027fc8dfed772609ad9bd68f611b7e4ce73afa949f27084ad3a92b15fe
            ;;
        *) echo "Unknown kubectl version $1"; exit 1;
    esac

    printf -- "--build-arg KUBECTL_VERSION=%s " "$KUBECTL_VERSION"
    printf -- "--build-arg KUBECTL_DOWNLOAD_SHA256=%s " "${KUBECTL_DOWNLOAD_SHA256[$CUSTOM_DOCKER_ARCH]}"
}

function parse_arguments() {
    printf -- "-f Dockerfile.custom "

    # Mapping between sources of architectures
    # - arch: a current image architecture (or uname -m if matching host, but can be different)
    # - apk --print-arch: a alpine architecture for installed packages
    # - dpkg --print-architecture: a debian architecture for installed packages
    # - docker arch: a Docker Hub architecture for images
    #
    # arch    | apk --print-arch | dpkg --print-architecture | docker arch
    # x86_64  | x86_64           | amd64                     | amd64
    # armhf   | armhf            | armhf                     | arm32v7
    # aarch64 | aarch64          | arm64                     | arm64v8

    # defaults
    case $(arch) in
        x86_64) CUSTOM_DOCKER_ARCH=amd64 ;;
        aarch64) CUSTOM_DOCKER_ARCH=arm64v8 ;;
        *) echo "unknown architecture $(arch)"; exit 1;;
    esac

    CUSTOM_IMAGE_NAME=debian
    CUSTOM_IMAGE_VERSION=buster

    for tool in "${PATH_TOOLS[@]}" "${TAG_TOOLS[@]}"; do
        if [ -n "${!tool}" ]; then
            version="${!tool}"
            case "$tool" in
                ARCH) CUSTOM_DOCKER_ARCH=$version ;;
                DEBIAN) CUSTOM_IMAGE_VERSION=$version ;;
                RUBY) print_ruby_args $version ;;
                BUNDLER) print_bundler_args $version ;;
                GOLANG) print_golang_args $version ;;
                CHROME) print_chrome_args $version ;;
                DOCKER) print_docker_args $version ;;
                BUILDX) print_buildx_args $version ;;
                GIT) print_git_args $version ;;
                LFS) print_lfs_args $version ;;
                NODE) print_node_args $version ;;
                YARN) print_yarn_args $version ;;
                POSTGRESQL) print_postgres_args $version ;;
                GRAPHICSMAGICK) print_graphicsmagick_args $version ;;
                PGBOUNCER) print_pgbouncer_args $version ;;
                BAZELISK) print_bazelisk_args $version ;;
                GCLOUD) print_gcloud_args $version ;;
                KUBECTL) print_kubectl_args $version ;;
                *) echo "unknown tool $tool"; exit 1;;
            esac
        fi
    done

    CUSTOM_IMAGE_NAME=$CUSTOM_DOCKER_ARCH/$CUSTOM_IMAGE_NAME # ex. https://hub.docker.com/r/amd64/debian/

    printf -- "--build-arg CUSTOM_IMAGE_NAME=%s " "$CUSTOM_IMAGE_NAME"
    printf -- "--build-arg CUSTOM_IMAGE_VERSION=%s " "$CUSTOM_IMAGE_VERSION"
    printf -- "--build-arg DEBIAN_VERSION=%s " "$CUSTOM_IMAGE_VERSION"
}

function generate_command() {
    printf -- "docker buildx build "
    parse_arguments

    for i in "$@"
    do
        printf -- "%s " "$i"
    done

    printf -- "--push=${PUSH_CUSTOM_IMAGE:-false} "
    printf -- ".\\n"
}

function build_custom_if_needed() {
    build_image_path=$(get_image_path)
    build_image_tag=$(get_image_tag)
    full_image_name="$build_image_path:$build_image_tag"
    echo "Building image $full_image_name"

    docker_command=$(generate_command $@ --cache-to=type=inline --cache-from="$full_image_name" -t "$full_image_name")
    echo "Docker command:"
    printf "\t%s\n" "$docker_command"
    eval $docker_command
    printf "\n\nSUCCESS - Successfully built:\n\t%s\n" "$full_image_name"
}

build_custom_if_needed $@

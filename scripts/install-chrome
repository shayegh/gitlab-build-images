#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

if [[ $(dpkg --print-architecture) == arm64 ]]; then
    echo "The arm64 does not have prebuilt chrome. Using chromium instead."
    apt-get update -q -y
    apt-get install -y chromium chromium-driver
    apt-get autoremove -yq
    apt-get clean -yqq
    rm -rf /var/lib/apt/lists/*
    exit 0
fi

CHROME_VERSION=${1:-99.0.4844.74-1}
CHROME_DRIVER_VERSION=${2:-99.0.4844.51}
# We hard code the URL rather than using $CI_API_V4_URL $CI_PROJECT_ID,
# because we would need to forward those variables
CHROME_DOWNLOAD_URL_BASE="https://gitlab.com/api/v4/projects/1075790/packages/generic/google-chrome-stable"
export DEBIAN_FRONTEND=noninteractive

curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list

apt-get update -q -y

# Download from our package registry if we can't find the package in the apt repository
echo "Searching for $CHROME_VERSION in apt repository"
CHECK_VERSION=$(apt-cache show google-chrome-stable | grep Version | grep "$CHROME_VERSION") || true

apt-cache policy google-chrome-stable

if [[ -z $CHECK_VERSION ]]; then
    CHROME_DEB="google-chrome-stable_${CHROME_VERSION}_$(dpkg --print-architecture).deb"
    CHROME_URL="${CHROME_DOWNLOAD_URL_BASE}/${CHROME_VERSION}/${CHROME_DEB}"
    echo "Downloading from our Package registry: $CHROME_URL"
    curl --silent --show-error --fail -O $CHROME_URL
    dpkg -i ./$CHROME_DEB || true
    apt-get install -f -y
    rm -f $CHROME_DEB
else
    echo "Installing via apt-get"
    apt-get install -y google-chrome-stable=$CHROME_VERSION
fi

rm -rf /var/lib/apt/lists/*

# Install ChromeDriver
wget -q https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip
unzip chromedriver_linux64.zip -d /usr/local/bin
rm -f chromedriver_linux64.zip

apt-get autoremove -yq
apt-get clean -yqq
rm -rf /var/lib/apt/lists/*
rm -rf /etc/apt/sources.list.d/google*.list
